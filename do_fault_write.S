
.macro ventry label
	.align	7
	b	\label
.endm

	adr	x0, vectors
	msr	vbar_el1, x0
	isb

	adr	x0, ram
	mov	x3, #1234
1:	str	x3, [x0]
	mov	x30, x0
	add	x0, x0, #4096
	tst	x0, #(1 << 30) // 1GB
	b.eq	1b

die:
	mov	x0, #0x84000000
	add	x0, x0, #8
	hvc	#0
	b	.

	.align	11
vectors:
	ventry	die
	ventry	die
	ventry	die
	ventry	die
	ventry	die
	ventry	die
	ventry	die
	ventry	die
	ventry	die
	ventry	die
	ventry	die
	ventry	die
	ventry	die
	ventry	die
	ventry	die
	ventry	die

	.align	12
ram:
