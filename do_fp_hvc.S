
__start:
	mov	x19, #(1 << 20)
	mov	x0, #(3 << 20)
	msr	cpacr_el1, x0
	isb
1:	mov	x0, #0x84000000
	fmov	d0, x0
	hvc	#0
	sub	x19, x19, #1
	cbnz	x19, 1b
	mov	x0, #0x84000000
	add	x0, x0, #8
	hvc	#0
	b	.
