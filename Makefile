
LOOP_BINS := do_hvc.bin do_sgi.bin do_sysreg.bin do_fp_hvc.bin
MEMORY_BINS := do_fault_read.bin do_fault_write.bin do_fault_read_write.bin

BINS := $(LOOP_BINS) $(MEMORY_BINS)

PERF ?= perf
LKVM ?= lkvm
LKVM_ARGS ?=

%.bin : %.o
	objcopy -O binary $^ $@

all: $(BINS)

clean:
	rm -f $(BINS)

.PHONY: tests-gicv2 tests-gicv3 tests-memory

tests-gicv2:
	@echo GICv2:; for i in $(LOOP_BINS); do echo -n $$i: ; LANG='C' taskset -c 1 $(PERF) stat -e cycles:hk $(LKVM) run -c1 --firmware $$i --irqchip=gicv2 $(LKVM_ARGS) --pmu 2>&1 >/dev/null| grep cycles | awk '{print $$1 / 2^20}'; done

tests-gicv3:
	@echo GICv3:; for i in $(LOOP_BINS); do echo -n $$i: ; LANG='C' taskset -c 1 $(PERF) stat -e cycles:hk $(LKVM) run -c1 --firmware $$i --irqchip=gicv3 --pmu 2>&1 >/dev/null | grep cycles | awk '{print $$1 / 2^20}'; done

tests-memory:
	@echo Memory:; for i in $(MEMORY_BINS); do echo -n $$i: ; LANG='C' taskset -c 1 $(PERF) stat -e cycles:hk $(LKVM) run -c1 --firmware $$i -m 1024 2>&1 >/dev/null | grep elapsed; done

tests: $(BINS) tests-gicv2 tests-gicv3 tests-memory
